'use strict';
(function () {
	// Главный слайдер
	var changeSlide = function (element, direction) {
		var list = element.siblings('.list');
		var currItem = list.find('.active');
		var currTitle = element.siblings('.title');

		currItem.animate({
			'opacity': '0'
		}, 300);
		currTitle.animate({
			'opacity': '0'
		}, 300);

		if (direction == 'next') {
			setTimeout(function () {
				currItem.removeClass('active');
			}, 300);
			var nextItem;
			if (currItem.next('.item').length) {
				nextItem = currItem.next('.item');
			}
			else {
				nextItem = $(list.children()[0]);

			}

			setTimeout(function () {
				currTitle.html(nextItem.data('title'));
				currTitle.animate({'opacity': '1'}, 100);
			}, 300);

			nextItem.css('opacity', 0);
			nextItem.addClass('active');
			nextItem.animate({'opacity': '1'}, 300);
		}
		else {
			setTimeout(function () {
				currItem.removeClass('active');
			}, 300);
			var nextItem;
			if (currItem.prev('.item').length) {
				nextItem = currItem.prev('.item');
			}
			else {
				nextItem = $(list.children()[list.children().length - 1]);
			}

			setTimeout(function () {
				currTitle.html(nextItem.data('title'));
				currTitle.animate({'opacity': '1'}, 100);
			}, 300);

			nextItem.css('opacity', 0);
			nextItem.addClass('active');
			nextItem.animate({'opacity': '1'}, 300);
		}
	};

	var $controls = $('.control');
	if ($controls.length) {
		$controls.on('click', function (e) {
			e.preventDefault();
			var elem = $(this);
			if (elem.hasClass('next')) {
				changeSlide($(this), 'next');
			}
			else {
				changeSlide($(this), 'prev');
			}
		});
	}


	// Средняя карусель
	// Обработка клика на стрелку влево
	$(document).on('click', ".carousel-button-left", function () {
		var carusel = $(this).parents('.carousel');
		left_carusel(carusel);
		return false;
	});
	$(document).on('click', ".carousel-button-right", function () {
		var carusel = $(this).parents('.carousel');
		right_carusel(carusel);
		return false;
	});
	function left_carusel(carusel) {
		var block_width = $(carusel).find('.carousel-block').outerWidth();
		$(carusel).find(".carousel-items .carousel-block").eq(-1).clone().prependTo($(carusel).find(".carousel-items"));
		$(carusel).find(".carousel-items").css({"left": "-" + block_width + "px"});
		$(carusel).find(".carousel-items .carousel-block").eq(-1).remove();
		$(carusel).find(".carousel-items").animate({left: "0px"}, 200);

	}

	function right_carusel(carusel) {
		var block_width = $(carusel).find('.carousel-block').outerWidth();
		$(carusel).find(".carousel-items").animate({left: "-" + block_width + "px"}, 200, function () {
			$(carusel).find(".carousel-items .carousel-block").eq(0).clone().appendTo($(carusel).find(".carousel-items"));
			$(carusel).find(".carousel-items .carousel-block").eq(0).remove();
			$(carusel).find(".carousel-items").css({"left": "0px"});
		});
	}

	$(function () {
		auto_right('.carousel:first');
	});

// Автоматическая прокрутка
	function auto_right(carusel) {
		setInterval(function () {
			if (!$(carusel).is('.hover'))
				right_carusel(carusel);
		}, 2000)
	}

// Навели курсор на карусель
	$(document).on('mouseenter', '.carousel', function () {
		$(this).addClass('hover')
	});
//Убрали курсор с карусели
	$(document).on('mouseleave', '.carousel', function () {
		$(this).removeClass('hover')
	});


	// Количество лицензий
	$('.minus', '.count').on('click', function (e) {
		var elem = $($('figure', '.start')[2]);
		var elemPrice = elem.find('.start__img-text.price>.value');
		var price = parseInt(elemPrice.html());
		var elemCount = $(this).parents('.count').find('.count');
		var oldCount = parseInt(elemCount.val());
		if (oldCount > 1) {
			elemCount.val(oldCount - 1);
			calcCost(price, elemCount.val());
		}
	});
	$('.plus', '.count').on('click', function (e) {
		var elem = $($('figure', '.start')[2]);
		var elemPrice = elem.find('.start__img-text.price>.value');
		var price = parseInt(elemPrice.html());
		var elemCount = $(this).parents('.count').find('.count');
		var oldCount = parseInt(elemCount.val());
		if (oldCount < 10) {
			elemCount.val(oldCount + 1);

			calcCost(price, elemCount.val());
		}
	});

	function calcCost(packagePrice, packagesCount) {
		var elemResult = $('.result', '.order');
		elemResult.html(packagePrice * packagesCount);
		$('.total', '.order').html(parseInt(elemResult.html()) + parseInt($($('figure', '.start')[0]).find('.start__img-text.price>.value').html()));
	}

	// Скроллинг новостей
	var newsList = $('.list', '.news');
	var allWidth = 0;
	newsList.find('.item').each(function(i, elem) {
		allWidth += $(elem).outerWidth(true);
	});
	newsList.width(allWidth);

	// Вверх
	$('.scrollbar').perfectScrollbar({
		'suppressScrollY': true,
		'maxScrollbarLength': 21
	});

	$('.up').on('click', function(e) {
		var elem = $(e.target);
		e.preventDefault();
		$('html, body').animate({scrollTop:0}, 'slow');
	});

	// Авторизация
	$('.auth').fancybox({
		type: 'inline'
	});

	// Галерея
	$('.carousel-block a').fancybox();

	// Выбор года или языка
	$('body').on('click', function (e) {
		var elem = $(e.target);

		if (elem.hasClass('year')) {
			var dropDown = elem.next('.list');

			if (dropDown.hasClass('active')) {
				dropDown.removeClass('active');
			}
			else {
				dropDown.addClass('active');
			}
		}
		else {
			$('.year', '.articles').next('.list').removeClass('active');
		}

		if (elem.hasClass('lang__choosen') || elem.parent().hasClass('lang__choosen')) {
			var list = $('.lang__list');

			if (list.hasClass('m--visible')) {
				list.removeClass('m--visible');
			}
			else {
				list.addClass('m--visible');
			}
		}
		else {
			$('.lang__list').removeClass('m--visible');
		}
	});

	// Нижние картинки
	$('.photos__item a').fancybox();
})();